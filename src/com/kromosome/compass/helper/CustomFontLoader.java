package com.kromosome.compass.helper;

import android.content.Context;
import android.graphics.Typeface;

/**
 * CustomFontLoader : returns the requested font from /assets/fonts directory
 * 
 * @author matg
 *
 */
public class CustomFontLoader {
	
	private static Typeface tf = null;
	private static boolean blnFontLoaded = false;
	
	// FontType has a direct mapping to fontPath. This is a much easier
	// variable for a developer to pass as opposed to the actual font's
	// name which can be a very long string.
	public enum FontType{
		FONT_NAME_1,
		FONT_NAME_2,
		FONT_NAME_3
	};
	
	// array of installed fonts located in assets/fonts folder
	// user's choice of FontType determines the Typeface returned
	private static String[] fontPath = {
	    "ITC Avant Garde CE Gothic Book.ttf"
	};
	
	/**
	 * CustomFontLoader Constructor
	 * @param c 	= 
	 * @param ft
	 */
	public CustomFontLoader(Context c, FontType ft){
		// check for an already loaded font resource
		//if(!blnFontLoaded){
			CustomFontLoader.loadFont(c, ft);
		//} else {
			// if font loaded, ensure it contains the required
			// font resource, else a reload is necessary
			//if(ft.)
			//if(CustomFontLoader.tf.equals(ft.)
		//}
	}

	/**
	 * Returns a loaded custom font based on it's fontType identifier. 
	 * 
	 * @param context 	= the current context
	 * @param fontType 	= the identifier of the requested font
	 * @return Typeface object of the requested font.
	 */
	public static Typeface getTypeface(Context c, FontType fontType) {

		if (!blnFontLoaded) {
	        CustomFontLoader.tf = loadFont(c, fontType);
	    }
		return CustomFontLoader.tf;
	}
	
	/**
	 * loadFont: returns the requested font from /assets/fonts directory
	 * @param context
	 * @param ft
	 * @return Typeface
	 */
	private static Typeface loadFont(Context c, FontType ft) {
		
		switch(ft){
			case FONT_NAME_1:
				CustomFontLoader.tf = Typeface.createFromAsset(c.getAssets(), "fonts/"+ CustomFontLoader.fontPath[0]);
				break;
			case FONT_NAME_2:
				CustomFontLoader.tf = Typeface.createFromAsset(c.getAssets(), "fonts/" + CustomFontLoader.fontPath[1]);
				break;
			case FONT_NAME_3:
				CustomFontLoader.tf = Typeface.createFromAsset(c.getAssets(), "fonts/" + CustomFontLoader.fontPath[2]);
				break;
		}
	    return CustomFontLoader.tf;
	}
}
